import os

from flask import Flask, redirect, url_for, session, request, jsonify
from flask_oauthlib.client import OAuth

app = Flask(__name__)
app.debug = True
app.secret_key = 'development'
oauth = OAuth(app)
oauth_client_id = os.getenv('OAUTH_CLIENT_ID')
oauth_secret_key = os.getenv('OAUTH_SECRET_KEY')

cern = oauth.remote_app(
    'cern',
    consumer_key=oauth_client_id,
    consumer_secret=oauth_secret_key,
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://oauth.web.cern.ch/OAuth/Token',
    authorize_url='https://oauth.web.cern.ch/OAuth/Authorize',
    )


@app.route('/')
def index():
    if 'cern_token' in session:
        me = cern.get('https://oauthresource.web.cern.ch/api/Groups')
        return jsonify(me.data)
    return redirect(url_for('login'))


@app.route('/login')
def login():
    return cern.authorize(callback=url_for('authorized', _external=True))


@app.route('/logout')
def logout():
    session.pop('cern_token', None)
    return redirect(url_for('index'))


@app.route('/oauth-done')
def authorized():
    resp = cern.authorized_response()
    if resp is None or resp.get('access_token') is None:
        return 'Access denied: reason=%s error=%s resp=%s' % (
            request.args['error'],
            request.args['error_description'],
            resp
        )
    session['cern_token'] = (resp['access_token'], '')

    me = cern.get('https://oauthresource.web.cern.ch/api/Groups')
    return jsonify(me.data)


@cern.tokengetter
def get_cern_oauth_token():
    return session.get('cern_token')
