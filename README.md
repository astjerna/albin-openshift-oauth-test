# What is this? #

This is a sample project using Flask and Python 3 to deploy an
application to CERN's OpenShift PAAS using CERN's central OAuth 2.0.

## Configuration

You need to register your app for use with CERN's central OAuth. As callback URL, enter https://your-app.cern.ch/oauth-done, probably something like https://my-web-app.web.cern.ch/oauth-done.

Additionally, you need to inject your OAuth secret key and client ID (from the OAuth registration page) using the environment variables `OAUTH_SECRET_KEY` and `OAUTH_CLIENT_ID` respectively.
