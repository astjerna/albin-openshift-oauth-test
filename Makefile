image:
	docker build --force-rm -t oauth-test .
run:
	docker run --env-file .env -it --rm --publish-all oauth-test
