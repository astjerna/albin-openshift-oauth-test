FROM python:3.6-alpine
MAINTAINER albin.stjerna@cern.ch
EXPOSE 8000

RUN apk add --no-cache gcc make musl-dev python-dev linux-headers

RUN pip install --upgrade pip
RUN pip install virtualenv uwsgi

RUN mkdir -p /opt/apps/test-app/
COPY . /opt/apps/test-app/
WORKDIR /opt/apps/test-app
RUN adduser -D appserver
RUN touch /var/log/app.log
RUN chown -R appserver . /var/log/app.log
USER appserver

RUN virtualenv --python=python3 venv
RUN source venv/bin/activate && pip install -r requirements.txt
CMD uwsgi uwsgi.ini
